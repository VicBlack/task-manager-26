package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    int getSize(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

}
