package ru.t1.kupriyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull Integer index);

    void removeAll();

    int getSize();

    boolean existsById(@NotNull String id);

}
