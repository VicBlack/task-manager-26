package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IRepository;
import ru.t1.kupriyanov.tm.api.service.IService;
import ru.t1.kupriyanov.tm.exception.entity.ModelNotFoundException;
import ru.t1.kupriyanov.tm.exception.field.IdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.IndexIncorrectException;
import ru.t1.kupriyanov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findOneById(id);
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(index);
        return removeOne(model);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
